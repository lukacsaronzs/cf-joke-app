component  output="false"
{

	public string function EncryptPassword(required string password)
	{
		return hash(password, "SHA-256", "UTF-8");
	}
	public struct function QueryToStruct(required any res)
	{
		return {
				id: int(res.id),
				name: res.name,
				email: res.email,
				password: res.password,
				created_at: res.created_at
			}
	}
	public struct function getUserByID(required numeric id) output="false"{
		sql = 'SELECT id, name, email, password, created_at FROM users WHERE users.id = :id;';
		params = {
					
					id: {
						value: id,
						type: "int"	
					}
		};
		options = {
			datasource: "jokeAppDataSource"
		}
		res=QueryExecute(sql, params, options);
		if (res.RecordCount() == 1){
			return this.QueryToStruct(res);
		}
		else{
			return {};

		}

	}

	public struct function getUserByEmail(required string userEmail) output="false"{
		sql = 'SELECT id, name, email, password, created_at FROM users WHERE users.email = :email;';
		params = {
					
					email: {
						value: userEmail,
						type: "varchar"	
					}
		};
		options = {
			datasource: "jokeAppDataSource"
		}
		res=QueryExecute(sql, params, options);
		if (res.RecordCount() == 1){
			return this.QueryToStruct(res);
		}
		else{
			return {};

		}

	}

	public any function getUserByCredentials(required string userEmail, required string userPassword){
		sql = 'SELECT id, name, email, password, created_at FROM users WHERE users.email = :email AND users.password=:password;';
		params = {
					
					email: {
						value: userEmail,
						type: "varchar"	
					},
					password: {
						value: this.EncryptPassword(userPassword),
						type: "varchar"
					}
		};
		options = {
			datasource: "jokeAppDataSource"
		}
		res=QueryExecute(sql, params, options);
		if (res.RecordCount() == 1){
			return this.QueryToStruct(res);
		}
		else{
			return {};

		}
	}
	
	public struct function addUser(required string userName,required string userEmail,required string userPassword) output="false"
	{	
		existing_user = this.getUserByEmail(userEmail);
		
		if (! StructIsEmpty(existing_user)){
			throw (type="ValidationError", message="Already exists with same email address!");
			return {};
		}

		validationErrors = this.validateUser(userName, userEmail, userPassword);
		if (! ArrayIsEmpty(validationErrors)){
			throw (type="ValidationError", message="Invalid data!");
			return {};

		}
		sql = 'insert into users(users.name, users.email, users.password) values (:name, :email, :password);';
		params = {
					name: {
						value: userName,
						type: "varchar"
					},
					email: {
						value: userEmail,
						type: "varchar"	
					},
					password: {
						value: this.EncryptPassword(userPassword),
						type: "varchar"
						
					}
		};
		options = {
			datasource: "jokeAppDataSource"
		}
		added=QueryExecute(sql, params, options);
        return this.getUserByEmail(userEmail);

	}

public any function deleteUserById(required any ID) output="false"
	{	
		
		sql = 'DELETE FROM users WHERE users.id=:id;'

		params = {
					id: {
						value: ID,
						type: "int"
					}
		};
		options = {
			datasource: "jokeAppDataSource"
		}
		added=QueryExecute(sql, params, options);
        

	}


	public array function validateUser(required string userName,required string userEmail,required string userPassword)
	output="false"
	{
		var aErrorMessages = [];
		// Validate name
		if (len(userName) < 5 OR len(userName) > 100) {
			arrayAppend(aErrorMessages,"Please provide a name between 5 and 100 characters!");
		}
		// Validate email address

		if (userEmail == '' OR !isValid("eMail", userEmail) ) {
			arrayAppend(aErrorMessages,"Please provide a valid email address!");
		}
		// Validate password

		if (len(userPassword) < 5 OR len(userPassword) > 100) {
			arrayAppend(aErrorMessages,"Please enter a password between 5 and 100 characters!");
		}
		return aErrorMessages;
		}

}