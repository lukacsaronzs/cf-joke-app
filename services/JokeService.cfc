component output = "false"{
    

	baseURL = "https://api.chucknorris.io/jokes";




	private any function apiCallGetCategories(){
			http url=baseURL & "/categories" method="get" result="res";
		return res;

	

	}
	private any function apiCallRandomJoke(category){
	http url=baseURL & "/random" method="get" result="res"{
		};
		return res;
	}

	private any function apiCallRandomJokeWithCategory(category){
	http url=baseURL & "/random" method="get" result="res"{
			httpparam type="url" name="category" value=#category#;

		};
		return res;
	}

	
    remote array function getJokeCategories() output="false"   
	{	
		response = apiCallGetCategories();

		if (response.status_code == 200) {
			response = deserializejson(variables.response.filecontent);
	
			return response;		

		}
		else{
			return [];
		}
		

	}


	remote struct function getRandomJoke() output="false"
	{
		response = apiCallRandomJoke();
		if (response.status_code == 200) {
			return deserializejson(variables.response.filecontent);;
		}
		else{
			return {};
		}
	}

	remote struct function getRandomJokeWithCategory(category) output="false"
	{	
		if (category == "")
		{
			return this.getRandomJoke();
		}
		response = apiCallRandomJokeWithCategory(category);
		if (response.status_code == 200) {
			 return deserializejson(variables.response.filecontent);;
		}
		else{
			return {};
		}
	}

	


}