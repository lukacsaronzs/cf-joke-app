component  output="false"
{

	remote array function AddRating(required string joke_url, required string user_id, required string rating){
			aErrors = [];
			try{
				user_id_int = Int(user_id);
				rating_int = Int(rating);
				res = dbAddRating(joke_url, user_id_int, rating_int);
				return aErrors;
			}
			catch(Exception ex){
				ArrayAppend(aErrors, "Invalid parameters.")
				return aErrors;
			}
	}

	public any function GetRatingsByUser(required numeric user_id){
			aErrors = [];
			try{
				user_id_int = Int(user_id);		
				return dbGetRatingsByUser(user_id_int);
			}
			catch(Exception ex){
				ArrayAppend(aErrors, "Invalid parameters.")
				return aErrors;
			}
	}

	remote array function UpdateRating(required string id, required string joke_url, required string user_id, required string rating){
			aErrors = [];
			try{
				id_int = Int(id)
				user_id_int = Int(user_id);
				rating_int = Int(rating);
				res = dbUpdateRating(id_int,joke_url, user_id_int, rating_int);
				return aErrors;
			}
			catch(Exception ex){
				ArrayAppend(aErrors, "Invalid parameters.")
				return aErrors;
			}
	}
	public any function DeleteRating(required string id){
				aErrors = [];
				try{
					id_int = Int(id);		
					return dbDeleteRating(id_int);
				}
				catch(Exception ex){
					ArrayAppend(aErrors, "Invalid parameters.")
					return aErrors;
				}
	}


	private void function dbAddRating(required string joke_url, required numeric user_id, required numeric rating)
	{
		sql = 'insert into chuck_joke_ratings(rating, url, user_id) values (:rating, :joke_url, :user_id);';
		params = {
					rating: {
						value: int(rating),
						type: "int"
					},
					joke_url: {
						value: joke_url,
						type: "varchar"	
					},
					user_id: {
						value: int(user_id),
						type: "int"	
					},
			};
		options = {
			datasource: "jokeAppDataSource"
		};
		try{
			added=QueryExecute(sql, params, options);
			return added.recordcount;
		}
		catch(Exception ex){
			throw ex;
		}
		
	}
	private void function dbUpdateRating(required numeric id, required string joke_url, required numeric user_id, required numeric rating)
	{
		sql = 'update chuck_joke_ratings set rating = :rating, url = :joke_url, user_id = :user_id WHERE id = :id;';
		params = {
					id: {
						value: id,
						type: "int"
					},
					rating: {
						value: rating,
						type: "int"
					},
					joke_url: {
						value: joke_url,
						type: "varchar"	
					},
					user_id: {
						value: user_id,
						type: "int"
						
					}
		};
		options = {
			datasource: "jokeAppDataSource"
		}
		added=QueryExecute(sql, params, options);
		return added.recordcount;
	}
	private void function dbDeleteRating(required numeric id)
	{
		sql = 'DELETE FROM chuck_joke_ratings WHERE id = :id;';
		params = {
					id: {
						value: id,
						type: "int"
					}
		}
		options = {
			datasource: "jokeAppDataSource"
		}
		added=QueryExecute(sql, params, options);
		return added.recordcount;
	}
	private any function dbGetRatingById(required numeric id)
	{
		sql = 'SELECT * FROM chuck_joke_ratings WHERE user_id = :id;';
		params = {
					id: {
						value: id,
						type: "int"
					}
		}
		options = {
			datasource: "jokeAppDataSource",
		}
		added=QueryExecute(sql, params, options);
		return added;
	}
	
	private any function dbGetRatingsByUser(required numeric id)
	{
		sql = 'SELECT * FROM chuck_joke_ratings WHERE user_id = :id';
		params = {
					id: {
						value: id,
						type: "int"
					}
		}
		options = {
			datasource: "jokeAppDataSource",
			returnType: "array"
		}
		added=QueryExecute(sql, params, options);
		return added;
	}


	public array function validateRating(required numeric userId,required string url, required numeric rating){
		var aErrorMessages = [];
		if (rating < 0 OR rating > 5) {
			arrayAppend(aErrorMessages,"Please provide a rating between 0 and 5!");
		}
		user = application.userService.getUserByID(userId)
		if (StructIsEmpty(user) ) {
			arrayAppend(aErrorMessages,"Please provide a valid user id!");
		}
		return aErrorMessages;
	}

	
  
    

	

}