<cfinclude template = "includes/accesscontrol.cfm">
<cfinclude template = "includes/head.cfm">

<a href="/index.cfm"><button>Jokes</button></a>
<a href="/login.cfm?logout"><button>Logout</button></a>


<cfif NOT structKeyExists(variables,'ratings')>
        <cfset ratings = application.ratingService.GetRatingsByUser(session.loggedInUser.id)/>
    </cfif>


<div>
    <p><cfoutput >Welcome #session.loggedInUser.name# !</cfoutput></p>

       <h4>Here are your previous joke ratings:</h4>
       <cfloop collection = "#ratings#" item = "rating">
            <cfoutput> 
            #ratings[rating].created_at#: <a href='#ratings[rating].url#'>url</a>
             Rating:  
             <select class="ratings">

             <cfloop index = "i" from = "1" to = "5">
                <cfif i eq #ratings[rating].rating# >
                    <option value="#ratings[rating].rating#"  selected>#ratings[rating].rating#</option>
                <cfelse>
                    <option value="i">#i#</option>

                </cfif>
             </cfloop>
            </select>
             
            <button onclick="Update(#ratings[rating].id#)">Update</button>
             <a href="/delete.cfm?ratingID=#ratings[rating].id#"><button>Delete</button></a>
            </cfoutput>
            <br>
       </cfloop>


        
    </div>
   
<script>
function Update(id){
    var rating = $('.ratings').find(":selected").text();
    console.log(rating);
    var url =   '<cfoutput>#ratings[rating].url#</cfoutput>';
  $.ajax({
            type:"GET",
            url:`/services/RatingService.cfc?method=UpdateRating&id=${id}&joke_url=${url}&user_id=${<cfoutput>#session.loggedInUser.id#</cfoutput>}&rating=${rating}&returnformat=JSON`,
            success: function(data) {
                console.log(data);
            },
            error: function(err){
                console.log(err);
            }
        });
}


</script>

</body>