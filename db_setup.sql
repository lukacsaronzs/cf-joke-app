CREATE TABLE users (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
	password VARCHAR(500) NOT NULL,
	email VARCHAR (100) NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    primary key(id)
);

CREATE TABLE chuck_joke_ratings (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
    url VARCHAR(500) NOT NULL,
	rating INT NOT NULL DEFAULT 0,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
    primary key(id),
    foreign key(user_id) references users(id)
);

insert into users(users.name, users.email, users.password) values ("testname", "test@email.com", "testpassword");


