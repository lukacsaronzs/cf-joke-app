component output = "false" {
	this.name = "cfJokeApp";
	this.datasource = "jokeAppDataSource";
	
	//Application start method
	function onApplicationStart() {
	application.jokeService = createObject("component", 'services.JokeService' );
	application.userService = createObject("component", 'services.UserService' );
	application.ratingService = createObject("component", 'services.RatingService');
	application.authenticationService = createObject("component", 'services.AuthenticationService' );
	return true;
	}
	
//onRequestStart() method
	boolean function onRequestStart() {
	
	//Handle special URL parameters
		if ( isDefined('url.restartApp') ) {
			this.onApplicationStart();
			}	
		return true;
	}
}
