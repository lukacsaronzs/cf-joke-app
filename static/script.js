var categories = [''];
var joke;


$( document ).ready(function() {
        $.ajax({
            type:"GET",
            url:"/services/JokeService.cfc?method=GetRandomJoke&returnformat=JSON",
            success: function(data) {
                joke = data;
                $('.joke').html(joke.value);
                $('.image').attr("src",joke.icon_url);


                },
            error: function(err){
                 console.log(err);
                }
            });
        $.ajax({
            type:"GET",
            url:"/services/JokeService.cfc?method=getJokeCategories&returnformat=JSON",
            success: function(data) {

                var catOptions = "<option value=''>" + 'Select' + "</option>";

                for (cat in data) {
                    
                    catOptions += "<option>" + data[cat] + "</option>";
                }
                $('.categories').html(catOptions);

            },
            error: function(err){
                console.log(err);
            }
        });
    });



function NewJoke(){
        var category = $('.categories').val();
        $.ajax({
            type:"GET",
            url:"/services/JokeService.cfc?method=GetRandomJokeWithCategory&category=" + category +"&returnformat=JSON",
            success: function(data) {

                joke = data;
                console.log(data);
                $('.joke').html(joke.value);
                $('.image').attr("src",joke.icon_url);


            },
            error: function(err){
                console.log(err);
            }
        });
}
